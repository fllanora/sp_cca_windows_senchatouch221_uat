Ext.define('app.view.ccaAboutView', {
    extend: 'Ext.Panel',
    xtype: 'ccaAboutView',
    
   requires: [
       'Ext.TitleBar'
   ],
    
    config: {
        title: 'About',
		   id: 'AboutView',
       //   iconCls: 'info',
	      iconCls: 'info win',
	  
        scrollable: 'vertical',
        styleHtmlContent: true,
	    style: 'background-color:#FFFFFF',

		
        items:[{
            xtype: 'titlebar',
            title: 'About',
            docked: 'top',
			     listeners: {
            

        initialize: function(){
		
				
	        Ext.Ajax.request({
            url: "http://fritzllanora.com/mobile/SP_AboutPage.html",
            success: function(response, opts){
                //var outputStrManifest = response.responseText;
					Ext.getCmp("AboutView").setHtml(response.responseText);
					
	         },
            failure: function(response, opts){
                //failCallback("fail");
            }
        });

          
		  	 
            	}
        },
               items: [
                       {
                       iconCls: 'settings',
                       ui: 'plain',
                       iconMask: true,
                       align: 'right',
					   id: 'settingsBtne',
					    	 listeners: {
            

        initialize: function(){ 
			if(device.platform=='Win32NT')
					   {
					  Ext.getCmp('settingsBtne').setIconCls('settings win');
					   }
		
		
		}},
                       
                       handler: function() {
                       displaySettingsPanel(this);
                       }}]

        }]

    }
});

