Ext.define('app.store.ccaFeedsItemStore', {
    extend: 'Ext.data.Store',


    requires: [
        'app.model.ccaFeedsItemModel'
    ],

    config: {
        model: 'app.model.ccaFeedsItemModel',
       autoLoad:false,
	 proxy: {
        type: 'ajax',
		 url : 'http://fritzllanora.com/mobile/cca_feeds.xml',
        //url : 'http://cca.sg/feed/',
// url: 'data/cca_feeds.xml',
        reader: {
            type: 'xml',
		    root  : 'channel',
            record: 'item'
        }
    }
	


    }
});