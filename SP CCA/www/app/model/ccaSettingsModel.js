Ext.define('app.model.ccaSettingsModel', {
           extend: 'Ext.data.Model',
           
           config: {
           fields: [
                    { name: 'index', type: 'string' },
                    { name: 'name', type: 'string' },
                    ]
           }
           });

