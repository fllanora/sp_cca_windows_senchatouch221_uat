
Ext.define('app.view.ccaFeedsView', {
    extend: 'Ext.Panel',
    xtype: 'ccaFeedsView',
 	requires: ['Ext.TitleBar', 
],
    
    config: { // config open
        title: 'CCA Feeds',
        iconCls: 'iconFeed',
		layout: 'vbox',
           
         items: [
			 {
				xtype: 'titlebar',			
				title: 'CCA Feeds',
				style: 'background-color:black;font-color:white;',
			/*	ui: 'tabcolour', */
                 docked: 'top', items: [
                                 {
                                 iconCls: 'settings',
                                 ui: 'plain',
                                 iconMask: true,
                                 align: 'right',
								    id: 'settingsBtnd',
								  	 listeners: {
            

        initialize: function(){ 
			if(device.platform=='Win32NT')
					   {
					  Ext.getCmp('settingsBtnd').setIconCls('settings win');
					   }
		
		
		}},
                                 
                                 handler: function() {
                                 displaySettingsPanel(this);
                                 }}]
			},
			{
                xtype: 'segmentedbutton',
				type : 'hbox',
				pack : 'start',
				align: 'stretchmax',
				docked: 'top',
                items: [{
					text: 'All clubs',
					id: 'AlignLeft',
				    width:'50%',
		            labelWidth : '100%',
					itemId: 'mybutton1'
			
			},
						{                  
                    text: 'Preferred Club',
					id: 'AlignRight',
					width:'50%',
		            labelWidth : '100%',
					itemId: 'mybutton2'					
						}
					] //segmentedbutton items
            },//segmentedbutton							
			{
			xtype: "list",
			id: 'feedsFilterTableList',
			name: 'feedsFilterTableList',
			requires: [       'app.store.ccaFeedsItemStore' ],
			emptyText: '',
	        store: 'ccaFeedsItemStore',
			//onItemDisclosure: true,
			layout: "fit", // take as much space as available
			flex: 1, // define flex

		/*		itemTpl:[
			
				'<div style="float:left; margin-left:5px; width:100%; margin-top:0px;">', '<span style="font-size:12px;">{title}</span><br/>', '<span style="font-size:9px;font-style:italic;color:gray;">Posted on: {pubDate:this.renderItem(values)}</span>', '</div>', '<div style="clear:both;">', '</div>',{
         
         */
                 
                 itemTpl:[
                          
       '<div style="float:left; margin-left:5px; width:100%;">', '<span style="font-size:12px;margin-right:25px;">{title}</span><img style="float:right; margin-right:10px;margin-top:10px;" src="resources/images/disclosure.png"  height="25" width="25"><br/>', '<span style="font-size:9px;font-style:italic;color:gray;">Posted on: {pubDate:this.renderItem(values)} </span>', '</div>', '<div style="clear:both;">', '</div>',{
                          
            
     
			compiled: true,
			renderItem: function(values){
			console.log("Values: "+values);

            //return values.format("D, d M Y H:i:s");
			return Ext.Date.format(values,Date.patterns.SPFormate);
        },
    }],
             
            listeners: {
                 itemtap: function(list, index, item, e){
                 
                 
                 var currentRecord = list.getStore().getAt(index);
                                
                 var groupLink = currentRecord.get('link');
                 window.open(groupLink, '_blank');

                 }
                 }
                 
                 
                 
                 
			}//list

        ],

        listeners: [
            {

                fn: 'onAlignLeftTap',

                event: 'tap',

                delegate: '#AlignLeft'

            },
            {

                fn: 'onAlignRightTap',

                event: 'tap',

                delegate: '#AlignRight'

            },

        ]

    },
  
    onAlignLeftTap: function(button, e, eOpts) {
          Ext.getCmp("feedsFilterTableList").setStore(Ext.create('app.store.ccaFeedsItemStore'));
         		 Ext.getCmp("feedsFilterTableList").getStore().load({url: 'http://cca.sg/feed/'} );
                 Ext.getCmp("feedsFilterTableList").refresh();
    },

    onAlignRightTap: function(button, e, eOpts) {

ccaFeedspicker.show();

    },

});



Date.patterns = {
    ISO8601Long: "Y-m-d H:i:s",
    ISO8601Short: "Y-m-d",
    ShortDate: "n/j/Y",
    LongDate: "l, F d, Y",
    FullDateTime: "l, F d, Y g:i:s A",
    MonthDay: "F d",
    ShortTime: "g:i A",
    LongTime: "g:i:s A",
    SortableDateTime: "Y-m-d\\TH:i:s",
    UniversalSortableDateTime: "Y-m-d H:i:sO",
    YearMonth: "F, Y",
    SPFormate: "D, d M Y H:i:s"
};

