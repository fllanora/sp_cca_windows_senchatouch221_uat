Ext.define('app.store.ccaSettingsStore', {
           extend: 'Ext.data.Store',
           
           
           requires: [
                      'app.model.ccaSettingsModel'
                      ],
           
           config: {
           model: 'app.model.ccaSettingsModel',
           autoLoad: true,
           data: [{index: '1', name: 'Logout'}],
           
           }
           });
