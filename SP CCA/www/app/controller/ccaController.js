var ccaGrade = "";//"SILVER";
var totalPoints = 0;//134;
var btnFlag = 'current';

var studentID = "122995";

var compItemArr = [];
var animArrValue = [];
var newAnimArrValue = [];
var additionalPointsReqd;
var componentTemplateFirstPage; 
var componentTemplateSecondPage

var topPageScorePanel;
var firstPageScorePanel;
var secondPageScorePanel;
var segmentedButton;
var ccaFeedspicker;
var ccaSettingsPanel;

var screenWidth = 0;

var my_media = null;

var el_info_button01_counter = 0;
var el_info_button02_counter = 0;
var el_info_button03_counter = 0;
var el_info_button04_counter = 0;
var el_info_button05_counter = 0;
var el_info_button06_counter = 0;
var el_info_button07_counter = 0;
var el_info_button08_counter = 0;

var animFlag;

try
{
    screenHeight = document.documentElement.clientHeight;
    screenWidth = document.documentElement.clientWidth;
}
catch(err){}
var screenTempHeight = 0;
var screenTempWidth = 0;
if (screenHeight <= 0) {
    try
    {
    screenHeight = window.innerHeight;
    }catch(err){}
}
if (screenWidth <= 0) {
    try
    {
    screenWidth = window.innerWidth;
    }catch(err){}
}

screenTempHeight = screenHeight;
screenTempWidth = screenWidth;

if (screenHeight < screenWidth) {
    screenHeight = screenTempWidth;
    screenWidth = screenTempHeight;
}


//  screenWidth = 600;


Ext.define('app.controller.ccaController', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            CCAScoresView: 'ccaScoresView',
            CCAGroupsList: 'ccaGroupsList',
            CCAFeedsView: 'ccaFeedsView',
            },
        
           control: {
           'ccaGroupsList': {
           itemtap: 'ccaGroupsListItemTap'
           },
           
           'ccaScoresView': {
           initialize: function(){
           try
           {
           studentID = SP.WebSSOPlugin.userID
           studentID = studentID.substring(1);
           }
           catch(err){}
           
           var screenHeight = 0;
           var screenWidth = 0;
           
           try
           {
            screenHeight = document.documentElement.clientHeight;
            screenWidth = document.documentElement.clientWidth;
           }
           catch(err){}
            var screenTempHeight = 0;
            var screenTempWidth = 0;
            if (screenHeight <= 0) {
            screenHeight = window.innerHeight;
            }
            if (screenWidth <= 0) {
            screenWidth = window.innerWidth;
            }
            
            screenTempHeight = screenHeight;
            screenTempWidth = screenWidth;
            
            if (screenHeight < screenWidth) {
            screenHeight = screenTempWidth;
            screenWidth = screenTempHeight;
            }
            
           // alert("screenHeight: "+screenHeight);
			// alert("screenWidth: "+screenWidth);

			// screenHeight = 
			//screenWidth = 700;

		//	screenWidth = 600;
  
           loadSoapRequest(this,studentID);
           drawScoreSegments(this);
          
           }
           
                      
           },
           


           'ccaFeedsView': {
           initialize: function(){
           try
           {
           Ext.getCmp("feedsFilterTableList").setStore(Ext.create('app.store.ccaFeedsItemStore'));
           //Ext.getCmp("feedsFilterTableList").getStore().load({url: 'http://cca.sg/feed/'} );
// Ext.getCmp("feedsFilterTableList").getStore().load({url: 'data/cca_feeds.xml'} );
		      Ext.getCmp("feedsFilterTableList").getStore().load({url: 'http://fritzllanora.com/mobile/cca_feeds.xml'} );

           Ext.getCmp("feedsFilterTableList").refresh();
           }
           catch(err){}
           
           ccaFeedspicker = Ext.create('Ext.Picker',{
                                           
                                           hidden: true,
                                           
                                           id: 'filterPicker',
                                           
                                           itemId: 'filterPicker',
                                           
                                           hideOnMaskTap: true,
                                           
                                           useTitles: true,
                                           
                                           doneButton: {
                                           
                                           itemId: 'Done'
                                           
                                           },
                                           
                                           cancelButton: {
                                           
                                           id: 'Cancel'
                                           
                                           },
                                           
                                           slots: [
                                                   
                                                   {
                                                   name: 'cca_clubs',
                                                   id: 'cca_clubs',
                                                   
                                                   store: Ext.create('app.store.ccaFeedsPickerStore'), //null, //'FeedsPickerStore',
                                                   valueField: 'url_addr',
                                                   displayField: 'name'
                                                   
                                                   }
                                                   
                                                   ],//slots
                                           listeners: {
                                           
                                           change: function(picker, objValue){
                                           
                                           try {
                                           } 
                                           catch (err) {
                                           alert("error in .getProxy().clear();");
                                           }
                                           Ext.getCmp("feedsFilterTableList").setStore(Ext.create('app.store.ccaFeedsFilterStore'));
                                           var feedUrl = 'http://' + picker.getValue()['cca_clubs'] + '/feed/';
                                           console.log("feedUrl: "+feedUrl);
                                           
                                           Ext.getCmp("feedsFilterTableList").getStore().load({url: feedUrl} );
                                           
                                           Ext.getCmp("feedsFilterTableList").refresh();
                                           },
                                           cancel: function(picker){
                                           }
                                           }
                                           
                                           });
           
           Ext.Viewport.add(ccaFeedspicker);
           }
           
           
           },
         
           },
           
           },
           
           
     launch: function(){

      },
           
    ccaGroupsListItemTap: function(list, index, target, record) {
           var groupLink = 'http://' + record.get('url_addr');
           window.open(groupLink, '_blank');
           }
});


/*****************************************************
 * Process NYAA Link in InAppBrowser
 *****************************************************/
function processNYAALink(){
    var nyaalinktap = Ext.get("nyaalinktap");
    nyaalinktap.addListener( 'tap', function(e){
                            console.log("nyaalinktap click");
                            window.open('http://www.nyaa.org', '_blank', 'location=no');
                            });
};

/*****************************************************
 * Display the Component List Panel
 *****************************************************/
function displayComponentListPanel(componentNum) {
    
    try
    {
     Ext.getCmp('ccaComponentsList').getStore().removeAll();
    }catch(err){}
    
    componentNum = componentNum -1;
    Ext.getCmp('ccaComponentsList').getStore().setData(compItemArr[componentNum].component_value);
    
    document.getElementById("machine").className = "";
    deActivateAnim();
    
    Ext.getCmp('ccaScoresCardView').animateActiveItem(1,{type: 'slide', direction: 'left'});
}



function displaySettingsPanel(button) {
    
    
    try{
        ccaSettingsPanel.destroy();
    }catch(err){}

	try
	{
	  Ext.getCmp('settingsPanel').destroy();
	  }
	  catch(err){}

	
    
    ccaSettingsPanel = Ext.create('Ext.Panel', {
                                  id: 'settingsPanel',
                                  hideOnMaskTap: true,
                                  modal: true,
                                  height: 52,
                                  width: 117,
                                  layout: {
                                  type: 'fit',
                                  pack: 'top',
                                  align: 'stretch'
                                  },
                                  items: [{xtype: 'button',
                                            text: 'Logout',
                                              ui: 'decline',
                                          height: 40,
                                           width: 105,
                                         handler: function(){
                                          
                                          Ext.getCmp('settingsPanel').hide();
                                          
										  if(device.platform=='Win32NT')
										  {
										   try{
        ccaSettingsPanel.destroy();
    }catch(err){}

	try
	{
	  Ext.getCmp('settingsPanel').destroy();
	  }
	  catch(err){}

	try
	{
	Ext.getCmp('settingsBtne').destroy();
	}
	catch(err){}

	try
	{
  Ext.getCmp('settingsBtnd').destroy();
  }
  catch(err){}

  try
  {
   Ext.getCmp('settingsBtnc').destroy();
   }
   catch(err){}

   try
   {
    Ext.getCmp('settingsBtnb').destroy();
	}
	catch(err){}

	try
	{
	 Ext.getCmp('settingsBtna').destroy();
	 }
	 catch(err){}


                         try
																										{

																										userIDLocalStorage.removeAll();
																										passwordLocalStorage.removeAll();
																										cookieLocalStorage.removeAll();
																										userIDLocalStorage.sync();
																										passwordLocalStoragesync();
																										cookieLocalStorage.sync();

																										}
																										catch(err){}
										  }
										  else
										  {
										                   try
                                          {
                                          SP.WebSSOPlugin.clearUserData();
                                          }
                                          catch(err){}
										  
										  }
                                          
                                          try
                                          {
                                          Ext.Viewport.removeAll(true,true);
                                          }
                                          catch(err){}
                                          
                                          Ext.Viewport.add(Ext.create('app.view.WebSSOLoginView'));
                                          Ext.Viewport.setActiveItem(1);
                                          
                                          }}],
                                  left: 0,
                                  padding: 10
                                  });
    
    ccaSettingsPanel.showBy(button);
}


function drawScoreSegments(controller){
    
    el_info_button01_counter = 0;
    el_info_button02_counter = 0;
    el_info_button03_counter = 0;
    el_info_button04_counter = 0;
    el_info_button05_counter = 0;
    el_info_button06_counter = 0;
    el_info_button07_counter = 0;
    el_info_button08_counter = 0;
    
    
    var htmlTemplate = '<br/><center><div id="machine" class="" style="margin-top: -5px;"><div id="a" class="anim_objects"></div><div id="b" class="anim_objects"></div><div id="c" class="anim_objects"></div></div><div id="machine_below" class="" style="display:none;"><br></div><div style="margin-top:10px; color:#fff;font-weight:bold;font-size:110%" id="ccaGradeDiv">Loading...</div></center>';

    if (screenWidth >= 700) {
        
        componentTemplateFirstPage = '<div id="content_sp1iPad"><br><div id="scorerow1iPad"><div style="height:20px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div id="div_name_1" style="float:left;margin-left:3px;margin-top:12px;">Leadership</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:12px;" id="barDiv_1"></div></div><img src="resources/images/black_cylinder.png" style="height:40px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top: -5px;"/><div id="bottomiPad"><div id="bottom1markeriPad" style="height:40px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: -8px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button01" style="background:url(resources/images/info_ipad.png) no-repeat; border:0px; height:39px;width:39px;position:absolute;float:left; margin-left:620px;margin-top:-38px;zIndex:999;"></button></div><div id="scorerow2iPad"><div style="height:20px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div id="div_name_2" style="float:left;margin-left:3px;margin-top:12px;">Participation</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:12px;" id="barDiv_2"></div></div><img src="resources/images/black_cylinder.png" style="height:40px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top: -5px;"/><div id="bottom2iPad"><div id="bottom2markeriPad" style="height:40px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: -8px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button02" style="background:url(resources/images/info_ipad.png) no-repeat; border:0px; height:39px;width:39px;position:absolute;float:right; margin-left:620px;margin-top:-38px;zIndex:-1;"></button></div><div id="scorerow3iPad"><div style="height:20px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div id="div_name_3" style="float:left;margin-left:3px;margin-top:12px;">Service</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:12px;" id="barDiv_3"></div></div><img src="resources/images/black_cylinder.png" style="height:40px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top: -5px;"/><div id="bottom3iPad"><div id="bottom3markeriPad" style="height:40px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: -8px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button03" style="background:url(resources/images/info_ipad.png) no-repeat; border:0px; height:39px;width:39px;position:absolute;float:right; margin-left:620px;margin-top:-38px;zIndex:-1;"></button></div><div id="scorerow4iPad"><div style="height:20px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div id="div_name_4" style="float:left;margin-left:3px;margin-top:12px;">Enrichment</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:12px;" id="barDiv_4"></div></div><img src="resources/images/black_cylinder.png" style="height:40px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top: -5px;"/><div id="bottom4iPad"><div id="bottom4markeriPad" style="height:40px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: -8px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button04" style="background:url(resources/images/info_ipad.png) no-repeat; border:0px; height:39px;width:39px;position:absolute;float:right; margin-left:620px;margin-top:-38px;zIndex:-1;"></button></div><div id="scorerow5iPad"><div style="height:20px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div style="float:left;margin-left:3px;margin-top:12px;">Representation</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:12px;" id="barDiv_5"></div></div><img src="resources/images/black_cylinder.png" style="height:40px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top: -5px;"/><div id="bottom5iPad"><div id="bottom5markeriPad" style="height:40px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: -8px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button05" style="background:url(resources/images/info_ipad.png) no-repeat; border:0px; height:39px;width:39px;position:absolute;float:right; margin-left:620px;margin-top:-38px;zIndex:-1;"></button></div><div id="scorerow6iPad"><div style="height:20px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div style="float:left;margin-left:3px;margin-top:12px;">Community Svcs.</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:12px;" id="barDiv_6"></div></div><img src="resources/images/black_cylinder.png" style="height:40px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top: -5px;"/><div id="bottom6iPad"><div id="bottom6markeriPad" style="height:40px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: -8px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button06" style="background:url(resources/images/info_ipad.png) no-repeat; border:0px; height:39px;width:39px;position:absolute;float:right; margin-left:620px;margin-top:-38px;zIndex:-1;"></button></div><div id="scorerow7iPad"><div style="height:20px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div style="float:left;margin-left:3px;margin-top:12px;">Achievement</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:12px;" id="barDiv_7"></div></div><img src="resources/images/black_cylinder.png" style="height:40px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top: -5px;"/><div id="bottom7iPad"><div id="bottom7markeriPad" style="height:40px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: -8px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button07" style="background:url(resources/images/info_ipad.png) no-repeat; border:0px; height:39px;width:39px;position:absolute;float:right; margin-left:620px;margin-top:-38px;zIndex:-1;"></button></div><div id="scorerow8iPad"><div style="height:20px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div style="float:left;margin-left:3px;margin-top:12px;">Competition</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:12px;" id="barDiv_8"></div></div><img src="resources/images/black_cylinder.png" style="height:40px; width:460px; position:absolute; margin-left:150px; zIndex:99; margin-top: -5px;"/><div id="bottom8iPad"><div id="bottom8markeriPad" style="height:40px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: -8px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button08" style="background:url(resources/images/info_ipad.png) no-repeat; border:0px; height:39px;width:39px;position:absolute;float:right; margin-left:620px;margin-top:-38px;zIndex:-1;"></button></div><div id="scorerowSpace"></div><div id="scorerowText" ></div><ul id="bar"><li id="list_1"></li><li id="list_2"></li></ul></div>';
        
        firstPageScorePanel = Ext.create('Ext.Panel', {
                                         html: componentTemplateFirstPage,
                                         itemId: 'firstPageScorePanel'
                                         });
        
        Ext.getCmp('topScores').setHtml(htmlTemplate);
        Ext.getCmp('bottomScores').setWidth(768);
    }
    else
    {
        componentTemplateFirstPage = '<div id="content_sp1"><br><div id="scorerow1"><div style="height:20px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div id="div_name_1" style="float:left;margin-left:3px;margin-top:-5px;">Leadership</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:-5px;" id="barDiv_1"></div></div><img src="resources/images/black_cylinder.png" style="height:25px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top: -5px;"/><div id="bottom"><div id="bottom1marker" style="height:15px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: 0px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button01" style="background:url(resources/images/info.png) no-repeat; border:0px; height:29px;width:29px;position:absolute;float:left; margin-left:280px;margin-top:-25px;zIndex:999;"></button></div><div id="scorerow2"><div style="height:20px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div id="div_name_2" style="float:left;margin-left:3px;margin-top:-5px;">Participation</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:-5px;" id="barDiv_2"></div></div><img src="resources/images/black_cylinder.png" style="height:25px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top: -5px;"/><div id="bottom2"><div id="bottom2marker" style="height:15px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: 0px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button02" style="background:url(resources/images/info.png) no-repeat; border:0px; height:29px;width:29px;position:absolute;float:right; margin-left:280px;margin-top:-25px;zIndex:-1;"></button></div><div id="scorerow3"><div style="height:20px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div id="div_name_3" style="float:left;margin-left:3px;margin-top:-5px;">Service</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:-5px;" id="barDiv_3"></div></div><img src="resources/images/black_cylinder.png" style="height:25px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top: -5px;"/><div id="bottom3"><div id="bottom3marker" style="height:15px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: 0px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button03" style="background:url(resources/images/info.png) no-repeat; border:0px; height:29px;width:29px;position:absolute;float:right; margin-left:280px;margin-top:-25px;zIndex:-1;"></button></div><div id="scorerow4"><div style="height:20px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div id="div_name_4" style="float:left;margin-left:3px;margin-top:-5px;">Enrichment</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:-5px;" id="barDiv_4"></div></div><img src="resources/images/black_cylinder.png" style="height:25px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top: -5px;"/><div id="bottom4"><div id="bottom4marker" style="height:15px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: 0px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button04" style="background:url(resources/images/info.png) no-repeat; border:0px; height:29px;width:29px;position:absolute;float:right; margin-left:280px;margin-top:-25px;zIndex:-1;"></button></div><div id="scorerowSpace"></div><div id="scorerowText">&nbsp;*Participation Points is capped at 15 pts.</div><ul id="bar"><li id="list_1"></li><li id="list_2"></li></ul></div>';
        
        componentTemplateSecondPage = '<div id="content_sp2"><br><div id="scorerow5"><div style="height:20px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div style="float:left;margin-left:3px;margin-top:-5px;">Representation</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:-5px;" id="barDiv_5"></div></div><img src="resources/images/black_cylinder.png" style="height:25px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top: -5px;"/><div id="bottom5"><div id="bottom5marker" style="height:15px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: 0px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button05" style="background:url(resources/images/info.png) no-repeat; border:0px; height:29px;width:29px;position:absolute;float:right; margin-left:280px;margin-top:-25px;zIndex:-1;"></button></div><div id="scorerow6"><div style="height:20px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div style="float:left;margin-left:3px;margin-top:-5px;">Community Svcs.</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:-5px;" id="barDiv_6"></div></div><img src="resources/images/black_cylinder.png" style="height:25px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top: -5px;"/><div id="bottom6"><div id="bottom6marker" style="height:15px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: 0px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button06" style="background:url(resources/images/info.png) no-repeat; border:0px; height:29px;width:29px;position:absolute;float:right; margin-left:280px;margin-top:-25px;zIndex:-1;"></button></div><div id="scorerow7"><div style="height:20px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div style="float:left;margin-left:3px;margin-top:-5px;">Achievement</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:-5px;" id="barDiv_7"></div></div><img src="resources/images/black_cylinder.png" style="height:25px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top: -5px;"/><div id="bottom7"><div id="bottom7marker" style="height:15px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: 0px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button07" style="background:url(resources/images/info.png) no-repeat; border:0px; height:29px;width:29px;position:absolute;float:right; margin-left:280px;margin-top:-25px;zIndex:-1;"></button></div><div id="scorerow8"><div style="height:20px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top:25px; color:#fff; font-size:13px;"><div style="float:left;margin-left:3px;margin-top:-5px;">Competition</div><div style="float:right;font-style:italic;margin-right:3px;margin-top:-5px;" id="barDiv_8"></div></div><img src="resources/images/black_cylinder.png" style="height:25px; width:240px; position:absolute; margin-left:36px; zIndex:99; margin-top: -5px;"/><div id="bottom8"><div id="bottom8marker" style="height:15px; width:0px; position:absolute; margin-left:0px; zIndex:50; margin-top: 0px;"></div><div class="infobox"></div></div><button type="button" class="info_button" id="info_button08" style="background:url(resources/images/info.png) no-repeat; border:0px; height:29px;width:29px;position:absolute;float:right; margin-left:280px;margin-top:-25px;zIndex:-1;"></button></div><ul id="bar"></ul></div>';
        
        
        firstPageScorePanel = Ext.create('Ext.Panel', {
                                         html: componentTemplateFirstPage,
                                         itemId: 'firstPageScorePanel'
                                         });
        
        
        
        secondPageScorePanel = Ext.create('Ext.Panel', {
                                          html: componentTemplateSecondPage,
                                          itemId: 'secondPageScorePanel'
                                          });
        
        Ext.getCmp('topScores').setHtml(htmlTemplate);
        Ext.getCmp('bottomScores').setWidth(320);
        //  Ext.getCmp('bottomScores').setHeight(210);
    }
    
    if (btnFlag == 'current') {
        
        if (screenWidth >= 700)
        {
            Ext.getCmp('bottomScores').add(firstPageScorePanel);
        }else
        {
            Ext.getCmp('bottomScores').add(firstPageScorePanel);
            Ext.getCmp('bottomScores').add(secondPageScorePanel);
        }
        
        
    }
    else
        if (btnFlag == 'none') {
            Ext.getCmp('bottomScores').add(firstPageScorePanel);
        }
    
    setTimeout(function(){
               
               
               try {
               var el_info_button01 = Ext.get("info_button01");
               el_info_button01.addListener('tap', function(e){
                                            if (el_info_button01_counter == 0) {
                                            el_info_button01_counter = 1;
                                            console.log("info_button01 click");
                                            displayComponentListPanel(1);
                                            }
                                            
                                            });
               var el_info_button02 = Ext.get("info_button02");
               el_info_button02.addListener('tap', function(e){
                                            if (el_info_button02_counter == 0) {
                                            el_info_button02_counter = 1;
                                            console.log("info_button02 click");
                                            
                                            displayComponentListPanel(2);
                                            }
                                            
                                            });
               var el_info_button03 = Ext.get("info_button03");
               el_info_button03.addListener('tap', function(e){
                                            if (el_info_button03_counter == 0) {
                                            el_info_button03_counter = 1;
                                            console.log("info_button03 click");
                                            displayComponentListPanel(3);
                                            }
                                            });
               var el_info_button04 = Ext.get("info_button04");
               el_info_button04.addListener('tap', function(e){
                                            if (el_info_button04_counter == 0) {
                                            el_info_button04_counter = 1;
                                            console.log("info_button04 click");
                                            displayComponentListPanel(4);
                                            }
                                            });
               var el_info_button05 = Ext.get("info_button05");
               el_info_button05.addListener('tap', function(e){
                                            if (el_info_button05_counter == 0) {
                                            el_info_button01_counter = 1;
                                            console.log("info_button05 click");
                                            displayComponentListPanel(5);
                                            }
                                            });
               var el_info_button06 = Ext.get("info_button06");
               el_info_button06.addListener('tap', function(e){
                                            if (el_info_button06_counter == 0) {
                                            el_info_button06_counter = 1;
                                            console.log("info_button06 click");
                                            displayComponentListPanel(6);
                                            }
                                            });
               
               
               var el_info_button07 = Ext.get("info_button07");
               el_info_button07.addListener('tap', function(e){
                                            if (el_info_button07_counter == 0) {
                                            el_info_button07_counter = 1;
                                            console.log("info_button07 click");
                                            displayComponentListPanel(7);
                                            }
                                            });
               var el_info_button08 = Ext.get("info_button08");
               el_info_button08.addListener('tap', function(e){
                                            if (el_info_button08_counter == 0) {
                                            el_info_button08_counter = 1;
                                            console.log("info_button08 click");
                                            displayComponentListPanel(8);
                                            }
                                            });
               } 
               catch (err) {
               }
               
               
               
               }, 1000);
    
    
}


function drawTopSegments(controller){

    var topItemsArr = [];
    
    if ((ccaGrade == 'NIL') && (totalPoints == 100)) {
        topItemsArr = [];
    }
    
    if ((ccaGrade == 'NON-REVAMPED') && (totalPoints == 100)) {
        topItemsArr = [];
    }
    
    else 
        if ((ccaGrade == 'NIL') && (totalPoints > 100)) {
            topItemsArr = [{
                text: 'Current',
                width: '20%',
                pressed: true
            }, {
                width: '20%',
                text: 'Bronze'
            }, {
                width: '20%',
                text: 'Silver'
            }, {
                width: '20%',
                text: 'Gold'
            }, {
                width: '20%',
                text: 'Honours'
            }];
        }
        
        
        else 
            if (ccaGrade == 'BRONZE') {
                topItemsArr = [{
                    text: 'Current',
                    width: '25%',
                    pressed: true
                }, {
                    width: '25%',
                    text: 'Silver'
                }, {
                    width: '25%',
                    text: 'Gold'
                }, {
                    width: '25%',
                    text: 'Honours'
                }];
            }
            
            else 
                if (ccaGrade == 'SILVER') {
                    topItemsArr = [{
                        text: 'Current',
                        width: '33%',
                        pressed: true
                    }, {
                        width: '33%',
                        text: 'Gold'
                    }, {
                        width: '33%',
                        text: 'Honours'
                    }];
                }
                
                else 
                    if (ccaGrade == 'GOLD') {
                        topItemsArr = [{
                            text: 'Current',
                            width: '50%',
                            pressed: true
                        }, {
                            width: '50%',
                            text: 'Honours'
                        }];
                    }
                    
                    else {
                        topItemsArr = [];
                    }
    
    var segmentedButton = Ext.create('Ext.SegmentedButton', {
            allowMultiple: false,
            docked: 'top',
            id: 'scoreSegmentedButton',
            pressedCls: 'pressedSegmentBtn',
            cls: 'segmentedBtn',
            layout: {
                type: 'hbox',
                pack: 'center',
                align: 'stretchmax'
            },
            items: topItemsArr,
            
            listeners: {
                toggle: function(container, button, pressed){
                    if (pressed) {
                    
                        if (button.getText() == "Current") {
                            if (ccaGrade == "BRONZE") {
                                document.getElementById("ccaGradeDiv").style.color = "#CD7F32";
                            }
                            else 
                                if (ccaGrade == "SILVER") {
                                    document.getElementById("ccaGradeDiv").style.color = "#C0C0C0";
                                }
                                else 
                                    if (ccaGrade == "GOLD") {
                                        document.getElementById("ccaGradeDiv").style.color = "#FFD700";
                                    }
                                    else 
                                        if (ccaGrade == "GOLD with HONOURS") {
                                            document.getElementById("ccaGradeDiv").style.color = "#FFD700";
                                        }
                                        else {
                                            document.getElementById("ccaGradeDiv").style.color = "#FFFFFF";
                                        }
                    document.getElementById("ccaGradeDiv").innerHTML = ccaGrade + ' ACHIEVED';
                      
                        }
                        else {
                        
                            //Ext.getCmp('bottomScores').removeAll();
     //  Ext.getCmp('bottomScores').removeAt(0); 
       //Ext.getCmp('bottomScores').remove(Ext.getCmp('bottomScores').getComponent('secondPageScorePanel'));
       //  Ext.getCmp('bottomScores').add({
         //               html: componentTemplateFirstPage
           //         });
          //  Ext.getCmp('bottomScores').setActiveItem(0); 
                   

                            document.getElementById("ccaGradeDiv").style.color = "#FFFFFF";
                         document.getElementById("ccaGradeDiv").innerHTML = 'Additional Points Required';
     
                        }
                        
                        
                        console.log("User toggled the '" + button.getText() + "' button: " + (pressed ? 'on' : 'off'));
                        
                        document.getElementById("machine").className = "";
                 //       deActivateAnim();
                        
                        var elemArr = document.getElementsByClassName("info_button");
                        
                        
                        
                        if (button.getText() == 'Current') {
                            btnFlag = 'current';
                            resetBottomContainer(ccaGrade);
                        }
                        
                        else {
                            btnFlag = 'none';
                            resetBottomContainer(button.getText().toUpperCase());
                            
                          
                        }
                        
                        for (var elemNum = 0; elemNum < elemArr.length; elemNum++) {
                            if (button.getText() == 'Current') {
                                elemArr[elemNum].style.display = 'block';
                                deactivateBtnFlag = 'current';
                                btnFlag = 'current';
                                
                            }
                            
                            else {
                                elemArr[elemNum].style.display = 'none';
                                deactivateBtnFlag = 'none';
                                btnFlag = 'none';
                                
                            }
                        }
                       
                        
                    }
                }
            }
        });
    
    if (topItemsArr && topItemsArr.length > 0) {
         controller.getCCAScoresView().add(segmentedButton);
    }
   
   // controller.getCCAScoresView().add(segmentedButton);
   // drawScoreSegments(this);
    
    btnFlag = 'current';
   // Ext.getCmp('HomeCCAScoresPanel').setActiveItem(1);
  //  resetBottomContainer(ccaGrade);

}


function displayCCAScore(points){
 
 
	if (points != 999) {
		var animNum1 = points;
	}
    
	else {
		var animNum1 = totalPoints;
	}	
    
	var actualNum = animNum1 - 100;
	var numArr;
    
	if (actualNum >= 100) {
		numArr = actualNum.toString().split('');		
	}	
    
	else {
		if (actualNum >= 10) {
			numArr = [0, actualNum.toString().split('')[0], actualNum.toString().split('')[1]];					
		}
        
		else {
			numArr = [0, 0, actualNum.toString().split('')[0]];					
		}
	}
    
	var a = 5 + (numArr[0] * 28);
	var b = 5 + (numArr[1] * 28);
	var c = 5 + (numArr[2] * 28);
    
    document.getElementById("machine").className = "test";
   
	document.getElementById("a").style.backgroundPosition = "0," + " 0 -" + a + "px";	
	document.getElementById("b").style.backgroundPosition = "0," + " 0 -" + b + "px";	
	document.getElementById("c").style.backgroundPosition = "0," + " 0 -" + c + "px";
    
    document.getElementById("ccaGradeDiv").innerHTML = ccaGrade + ' ACHIEVED';
 

}





function resetBottomContainer(btnGrade){


    if (totalPoints > 100) {
    
    
        if (btnFlag != 'none') {
            if ((ccaGrade == 'NIL') || (ccaGrade == 'GOLD with HONOURS')) {
                document.getElementById("ccaGradeDiv").innerHTML = ccaGrade;
            }
            else {
                document.getElementById("ccaGradeDiv").innerHTML = ccaGrade + ' ACHIEVED';
            }
        }
        
        else {
            //document.getElementById("ccaGradeDiv").innerHTML = 'Additional Points Required';		
        }
        
        
        var ledComp;
        var parComp;
        var serComp;
        var enrComp;
        var repComp;
        var cosComp;
        var achComp;
        var comComp;
        var otherComp = " more pts reqd";
        
        var otherComponents;
        var servicesComponent;
        var leaderShipComponent1;
        var leaderShipComponent2;
        
        if (btnGrade == 'NIL') {
            ledComp = " of 5";
            parComp = " of 10";
            serComp = " of 5";
            enrComp = " of 5";
            repComp = " of 5";
            cosComp = " of 5";
            achComp = " of 5";
            comComp = " of 5";
            
            if (totalPoints == 100) {
                compItemArr[0].total_points = 0;
                compItemArr[1].total_points = 0;
                compItemArr[2].total_points = 0;
                compItemArr[3].total_points = 0;
                compItemArr[4].total_points = 0;
                compItemArr[5].total_points = 0;
                compItemArr[6].total_points = 0;
                compItemArr[7].total_points = 0;
                
                
            }
            
            animArrValue[0] = parseFloat(compItemArr[0].total_points / 5);
            animArrValue[1] = parseFloat(compItemArr[1].total_points / 10.0);
            animArrValue[2] = parseFloat(compItemArr[2].total_points / 5);
            animArrValue[3] = parseFloat(compItemArr[3].total_points / 5);
            animArrValue[4] = parseFloat(compItemArr[4].total_points / 5);
            animArrValue[5] = parseFloat(compItemArr[5].total_points / 5);
            animArrValue[6] = parseFloat(compItemArr[6].total_points / 5);
            animArrValue[7] = parseFloat(compItemArr[7].total_points / 5);
            
            for (var num = 0; num < 8; num++) {
                if (animArrValue[num] > 1) {
                    animArrValue[num] = 1;
                }
            }
        }
        
        else 
            if (btnGrade == 'BRONZE') {
                if (btnFlag != 'none') {
                    ledComp = " of 5";
                    parComp = " of 10";
                    serComp = " of 5";
                    enrComp = " of 5";
                    repComp = " of 5";
                    cosComp = " of 5";
                    achComp = " of 5";
                    comComp = " of 5";
                    
                    animArrValue[0] = parseFloat(compItemArr[0].total_points / 5.0);
                    animArrValue[1] = parseFloat(compItemArr[1].total_points / 10.0);
                    animArrValue[2] = parseFloat(compItemArr[2].total_points / 5.0);
                    animArrValue[3] = parseFloat(compItemArr[3].total_points / 5.0);
                    animArrValue[4] = parseFloat(compItemArr[4].total_points / 5.0);
                    animArrValue[5] = parseFloat(compItemArr[5].total_points / 5.0);
                    animArrValue[6] = parseFloat(compItemArr[6].total_points / 5.0);
                    animArrValue[7] = parseFloat(compItemArr[7].total_points / 5.0);
                    
                    
                    for (var num = 0; num < 8; num++) {
                        if (animArrValue[num] > 1) {
                            animArrValue[num] = 1;
                        }
                    }
                }
                
                else {
                    parComp = " more pts reqd";
                    otherComp = " more pts reqd";
                    
                    
                    additionalPointsReqd = (parseFloat(compItemArr[0].total_points) +
                    parseFloat(compItemArr[1].total_points) +
                    parseFloat(compItemArr[2].total_points) +
                    parseFloat(compItemArr[3].total_points) +
                    parseFloat(compItemArr[4].total_points) +
                    parseFloat(compItemArr[5].total_points) +
                    parseFloat(compItemArr[6].total_points) +
                    parseFloat(compItemArr[7].total_points));
                    
                    if (parseFloat(compItemArr[1].total_points) > 10) {
                        additionalPointsReqd = 15 - (additionalPointsReqd - (parseFloat(compItemArr[1].total_points) - 10));
                        
                    }
                    else {
                        additionalPointsReqd = 15 - additionalPointsReqd;
                    }
                    
                    
                    
                    
                    if (otherComponents < 0) {
                        otherComponents = 0;
                    }
                    
                    
                    
                    otherComponents = additionalPointsReqd;
                    
                    newAnimArrValue[0] = otherComponents;
                    
                    
                    additionalPointsReqd = additionalPointsReqd + 100; // add 100 for slot animation
                    animArrValue[0] = (15 - otherComponents) / 15;
                    animArrValue[1] = 0;
                    animArrValue[2] = 0;
                    animArrValue[3] = 0;
                    animArrValue[4] = 0;
                    animArrValue[5] = 0;
                    animArrValue[6] = 0;
                    animArrValue[7] = 0;
                    
                    for (var num = 0; num < 8; num++) {
                        if (animArrValue[num] > 1) {
                            animArrValue[num] = 1;
                        }
                    }
                    
                    
                }
            }
            
            else 
                if (btnGrade == 'SILVER') {
                    if (btnFlag != 'none') {
                        ledComp = " of 13";
                        parComp = " of 15";
                        serComp = " of 13";
                        enrComp = " of 13";
                        repComp = " of 13";
                        cosComp = " of 2";
                        achComp = " of 13";
                        comComp = " of 13";
                        
                        
                        
                        animArrValue[0] = parseFloat(compItemArr[0].total_points / 13.0);
                        animArrValue[1] = parseFloat(compItemArr[1].total_points / 15.0);
                        animArrValue[2] = parseFloat(compItemArr[2].total_points / 13.0);
                        animArrValue[3] = parseFloat(compItemArr[3].total_points / 13.0);
                        animArrValue[4] = parseFloat(compItemArr[4].total_points / 13.0);
                        animArrValue[5] = parseFloat(compItemArr[5].total_points / 2.0);
                        animArrValue[6] = parseFloat(compItemArr[6].total_points / 13.0);
                        animArrValue[7] = parseFloat(compItemArr[7].total_points / 13.0);
                        
                        for (var num = 0; num < 8; num++) {
                            if (animArrValue[num] > 1) {
                                animArrValue[num] = 1;
                            }
                        }
                    }
                    
                    else {
                        parComp = " more pts reqd";
                        serComp = " more pts reqd";
                        otherComp = " more pts reqd";
                        
                        
                        additionalPointsReqd = (parseFloat(compItemArr[0].total_points) +
                        parseFloat(compItemArr[1].total_points) +
                        parseFloat(compItemArr[2].total_points) +
                        parseFloat(compItemArr[3].total_points) +
                        parseFloat(compItemArr[4].total_points) +
                        parseFloat(compItemArr[5].total_points) +
                        parseFloat(compItemArr[6].total_points) +
                        parseFloat(compItemArr[7].total_points));
                        
                        if (parseFloat(compItemArr[1].total_points) > 15) {
                            additionalPointsReqd = 30 - (additionalPointsReqd - (parseFloat(compItemArr[1].total_points) - 15));
                            
                        }
                        else {
                            additionalPointsReqd = 30 - additionalPointsReqd;
                        }
                        
                        
                        servicesComponent = 2 - parseFloat(compItemArr[5].total_points);
                        
                        if (otherComponents < 0) {
                            otherComponents = 0;
                        }
                        
                        if (servicesComponent < 0) {
                            servicesComponent = 0;
                        }
                        
                        if (additionalPointsReqd < 0) {
                        
                            additionalPointsReqd = servicesComponent;
                        }
                        
                        otherComponents = additionalPointsReqd - servicesComponent;
                        //	alert(otherComponents+" "+additionalPointsReqd+" "+servicesComponent)
                        
                        newAnimArrValue[0] = servicesComponent;
                        newAnimArrValue[1] = otherComponents;
                        
                        additionalPointsReqd = additionalPointsReqd + 100; // add 100 for slot animation
                        animArrValue[0] = parseFloat(compItemArr[5].total_points) / 2.0;
                        animArrValue[1] = (28 - otherComponents) / 28;
                        animArrValue[2] = 0;
                        animArrValue[3] = 0;
                        animArrValue[4] = 0;
                        animArrValue[5] = 0;
                        animArrValue[6] = 0;
                        animArrValue[7] = 0;
                        
                        
                        if (newAnimArrValue[2] == 0) {
                            animArrValue[2] = 1;
                        }
                        
                        
                        for (var num = 0; num < 8; num++) {
                            if (animArrValue[num] > 1) {
                                animArrValue[num] = 1;
                            }
                        }
                        
                        
                    }
                }
                
                else 
                    if (btnGrade == 'GOLD') {
                        if (btnFlag != 'none') {
                        
                            ledComp = " of 4 L / A";
                            parComp = " of 15";
                            serComp = " of 24";
                            enrComp = " of 24";
                            repComp = " of 24";
                            cosComp = " of 2";
                            achComp = " of 4 L / A";
                            comComp = " of 24";
                            
                            animArrValue[0] = parseFloat(compItemArr[0].total_points / 4.0);
                            animArrValue[1] = parseFloat(compItemArr[1].total_points / 15.0);
                            animArrValue[2] = parseFloat(compItemArr[2].total_points / 26.0);
                            animArrValue[3] = parseFloat(compItemArr[3].total_points / 26.0);
                            animArrValue[4] = parseFloat(compItemArr[4].total_points / 26.0);
                            animArrValue[5] = parseFloat(compItemArr[5].total_points / 2.0);
                            animArrValue[6] = parseFloat(compItemArr[6].total_points / 4.0);
                            animArrValue[7] = parseFloat(compItemArr[7].total_points / 26.0);
                            
                            for (var num = 0; num < 8; num++) {
                                if (animArrValue[num] > 1) {
                                    animArrValue[num] = 1;
                                }
                            }
                        }
                        
                        else {
                            parComp = " more pts reqd";
                            cosComp = " more pts reqd";
                            ledComp = " more pts reqd";
                            achComp = " more pts reqd";
                            
                            
                            
                            additionalPointsReqd = (parseFloat(compItemArr[0].total_points) +
                            parseFloat(compItemArr[1].total_points) +
                            parseFloat(compItemArr[2].total_points) +
                            parseFloat(compItemArr[3].total_points) +
                            parseFloat(compItemArr[4].total_points) +
                            parseFloat(compItemArr[5].total_points) +
                            parseFloat(compItemArr[6].total_points) +
                            parseFloat(compItemArr[7].total_points));
                            
                            if (parseFloat(compItemArr[1].total_points) > 15) {
                                additionalPointsReqd = 45 - (additionalPointsReqd - (parseFloat(compItemArr[1].total_points) - 15));
                            }
                            else {
                                additionalPointsReqd = 45 - additionalPointsReqd;
                            }
                            
                            
                            servicesComponent = 2 - parseFloat(compItemArr[5].total_points);
                            leaderShipComponent1 = 4 - ((parseFloat(compItemArr[0].total_points)) + (parseFloat(compItemArr[6].total_points)));
                            
                            if (otherComponents < 0) {
                                otherComponents = 0;
                            }
                            
                            if (servicesComponent < 0) {
                                servicesComponent = 0;
                            }
                            
                            if (leaderShipComponent1 < 0) {
                                leaderShipComponent1 = 0;
                            }
                            
                            if (additionalPointsReqd < 0) {
                                additionalPointsReqd = servicesComponent + leaderShipComponent1;
                                
                            }
                            
                            
                            otherComponents = additionalPointsReqd - servicesComponent - leaderShipComponent1;
                            
                            newAnimArrValue[0] = servicesComponent;
                            newAnimArrValue[1] = leaderShipComponent1;
                            newAnimArrValue[2] = otherComponents;
                            
                            additionalPointsReqd = additionalPointsReqd + 100; // add 100 for slot animation
                            animArrValue[0] = parseFloat(compItemArr[5].total_points) / 2.0;
                            animArrValue[1] = ((parseFloat(compItemArr[0].total_points)) + (parseFloat(compItemArr[6].total_points))) / 4.0;
                            animArrValue[2] = 1 - ((39 - otherComponents) / 39);
                            animArrValue[3] = 0;
                            animArrValue[4] = 0;
                            animArrValue[5] = 0;
                            animArrValue[6] = 0;
                            animArrValue[7] = 0;
                            
                            if (newAnimArrValue[2] == 0) {
                                animArrValue[2] = 1;
                            }
                            
                            
                            for (var num = 0; num < 8; num++) {
                                if (animArrValue[num] > 1) {
                                    animArrValue[num] = 1;
                                }
                            }
                            
                            
                        }
                    }
                    
                    else { //for HONOURS
                        if (btnFlag != 'none') {
                            ledComp = " of 6";
                            parComp = " of 15";
                            serComp = " of 19";
                            enrComp = " of 19";
                            repComp = " of 19";
                            cosComp = " of 6";
                            achComp = " of 6";
                            comComp = " of 19";
                            
                            animArrValue[0] = parseFloat(compItemArr[0].total_points / 6.0);
                            animArrValue[1] = parseFloat(compItemArr[1].total_points / 15.0);
                            animArrValue[2] = parseFloat(compItemArr[2].total_points / 19.0);
                            animArrValue[3] = parseFloat(compItemArr[3].total_points / 19.0);
                            animArrValue[4] = parseFloat(compItemArr[4].total_points / 19.0);
                            animArrValue[5] = parseFloat(compItemArr[5].total_points / 6.0);
                            animArrValue[6] = parseFloat(compItemArr[6].total_points / 6.0);
                            animArrValue[7] = parseFloat(compItemArr[7].total_points / 19.0);
                            
                            for (var num = 0; num < 8; num++) {
                                if (animArrValue[num] > 1) {
                                    animArrValue[num] = 1;
                                }
                            }
                        }
                        
                        else {
                            parComp = " more pts reqd";
                            cosComp = " more pts reqd";
                            ledComp = " more pts reqd";
                            achComp = " more pts reqd";
                            
                            additionalPointsReqd = (parseFloat(compItemArr[0].total_points) +
                            parseFloat(compItemArr[1].total_points) +
                            parseFloat(compItemArr[2].total_points) +
                            parseFloat(compItemArr[3].total_points) +
                            parseFloat(compItemArr[4].total_points) +
                            parseFloat(compItemArr[5].total_points) +
                            parseFloat(compItemArr[6].total_points) +
                            parseFloat(compItemArr[7].total_points));
                            
                            if (parseFloat(compItemArr[1].total_points) > 15) {
                                additionalPointsReqd = 46 - (additionalPointsReqd - (parseFloat(compItemArr[1].total_points) - 15));
                            }
                            else {
                                additionalPointsReqd = 46 - additionalPointsReqd;
                            }
                            
                            
                            servicesComponent = 6 - parseFloat(compItemArr[5].total_points);
                            leaderShipComponent1 = 6 - ((parseFloat(compItemArr[0].total_points)) + (parseFloat(compItemArr[6].total_points)));
                            
                            if (otherComponents < 0) {
                                otherComponents = 0;
                            }
                            
                            if (servicesComponent < 0) {
                                servicesComponent = 0;
                            }
                            
                            if (leaderShipComponent1 < 0) {
                                leaderShipComponent1 = 0;
                            }
                            
                            
                            if (additionalPointsReqd < 0) {
                            
                                additionalPointsReqd = servicesComponent + leaderShipComponent1;
                            }
                            
                            otherComponents = additionalPointsReqd - servicesComponent - leaderShipComponent1;
                            
                            newAnimArrValue[0] = servicesComponent;
                            newAnimArrValue[1] = leaderShipComponent1;
                            newAnimArrValue[2] = otherComponents;
                            
                            additionalPointsReqd = additionalPointsReqd + 100; // add 100 for slot animation
                            animArrValue[0] = parseFloat(compItemArr[5].total_points) / 6.0;
                            animArrValue[1] = ((parseFloat(compItemArr[0].total_points)) + (parseFloat(compItemArr[6].total_points))) / 6.0;
                            animArrValue[2] = 1 - ((34 - otherComponents) / 34);
                            animArrValue[3] = 0;
                            animArrValue[4] = 0;
                            animArrValue[5] = 0;
                            animArrValue[6] = 0;
                            animArrValue[7] = 0;
                            
                            
                            if (newAnimArrValue[2] == 0) {
                                animArrValue[2] = 1;
                            }
                            
                            
                            for (var num = 0; num < 8; num++) {
                                if (animArrValue[num] > 1) {
                                    animArrValue[num] = 1;
                                }
                            }
                            
                        }
                    }
        
        //snd = new Audio('app/views/media/scroll.mp3');
        //snd.loop = true;	
        //snd.play();
    //    myMedia.stop();
     //   myMedia.play();
        try
        {
        
        if ((device.platform == 'iPhone Simulator') ||
            (device.platform == 'iPhone') ||
            (device.platform == 'iPad Simulator') ||
            (device.platform == 'iPad') ||
            (device.platform == 'iOS')) {
            my_media = new Media('/app/view/media/scroll.mp3', null, null);
            try {
                my_media.stop();
                my_media.play();
            }
            catch (err) {
            }
        }
        else
            if (device.platform == 'Android') {
                my_media = new Media('/android_asset/www/app/view/media/scroll.mp3', null, null);
                try {
                    my_media.stop();
                    my_media.play();
                }
                catch (err) {
                }
            }
            }
            catch(err){}

        
        
        if (btnFlag != 'none') {
            // alert(ccaScoreBottomCarousel.items.length);
            
            
               //    alert(Ext.getCmp('bottomScores').getItems().length);
                if (Ext.getCmp('bottomScores').getItems().length == 2) {
                    if (screenWidth < 700)
                    {
                    Ext.getCmp('bottomScores').add({html: componentTemplateSecondPage,
        itemId: 'secondPageScorePanel'
                   });
                    }

                }
                
                
            document.getElementById("div_name_1").innerHTML = "Leadership";
            document.getElementById("div_name_2").innerHTML = "Participation";
            document.getElementById("div_name_3").innerHTML = "Service";
            document.getElementById("div_name_4").innerHTML = "Enrichment";
            
            document.getElementById("list_1").style.display = 'block';
            document.getElementById("list_2").style.display = 'block';
            
            document.getElementById("scorerowText").style.display = 'none';
            
            document.getElementById("barDiv_1").innerHTML = compItemArr[0].total_points + " pts"; // + ledComp;
            document.getElementById("barDiv_2").innerHTML = compItemArr[1].total_points + " pts";
            +parComp;
            document.getElementById("barDiv_3").innerHTML = compItemArr[2].total_points + " pts";
            +serComp;
            document.getElementById("barDiv_4").innerHTML = compItemArr[3].total_points + " pts";
            +enrComp;
            document.getElementById("barDiv_5").innerHTML = compItemArr[4].total_points + " pts";
            +repComp;
            document.getElementById("barDiv_6").innerHTML = compItemArr[5].total_points + " pts";
            +cosComp;
            document.getElementById("barDiv_7").innerHTML = compItemArr[6].total_points + " pts";
            +achComp;
            document.getElementById("barDiv_8").innerHTML = compItemArr[7].total_points + " pts";
            +comComp;
            
            
            if (compItemArr[0].total_points > 5) {
                document.getElementById("barDiv_1").innerHTML = compItemArr[0].total_points + ' pts'; //+ ledComp + " [Exceed]";
            }
            if (compItemArr[1].total_points > 10) {
                document.getElementById("barDiv_2").innerHTML = compItemArr[1].total_points + ' pts'; //+ parComp + " [Exceed]";
            }
            if (compItemArr[2].total_points > 5) {
                document.getElementById("barDiv_3").innerHTML = compItemArr[2].total_points + ' pts'; //+ serComp + " [Exceed]";
            }
            if (compItemArr[3].total_points > 5) {
                document.getElementById("barDiv_4").innerHTML = compItemArr[3].total_points + ' pts'; //+  enrComp + " [Exceed]";
            }
            if (compItemArr[4].total_points > 5) {
                document.getElementById("barDiv_5").innerHTML = compItemArr[4].total_points + ' pts'; //+ repComp + " [Exceed]";
            }
            if (compItemArr[5].total_points > 5) {
                document.getElementById("barDiv_6").innerHTML = compItemArr[5].total_points + ' pts'; //+ cosComp + " [Exceed]";
            }
            if (compItemArr[6].total_points > 5) {
                document.getElementById("barDiv_7").innerHTML = compItemArr[6].total_points + ' pts'; //+ achComp + " [Exceed]";
            }
            if (compItemArr[7].total_points > 5) {
                document.getElementById("barDiv_8").innerHTML = compItemArr[7].total_points + ' pts'; //+  comComp + " [Exceed]";
            }
            
            if (screenWidth >= 700) {
                document.getElementById("scorerow1iPad").style.display = 'block';
                document.getElementById("scorerow2iPad").style.display = 'block';
                document.getElementById("scorerow3iPad").style.display = 'block';
                document.getElementById("scorerow4iPad").style.display = 'block';
                document.getElementById("scorerow5iPad").style.display = 'block';
                document.getElementById("scorerow6iPad").style.display = 'block';
                document.getElementById("scorerow7iPad").style.display = 'block';
                document.getElementById("scorerow8iPad").style.display = 'block';
                
                document.getElementById("scorerowText").style.display = 'none';
                
                document.getElementById("bottom1markeriPad").style.display = 'none';
                document.getElementById("bottom2markeriPad").style.display = 'none';
                document.getElementById("bottom3markeriPad").style.display = 'none';
                document.getElementById("bottom4markeriPad").style.display = 'none';
                document.getElementById("bottom5markeriPad").style.display = 'none';
                document.getElementById("bottom6markeriPad").style.display = 'none';
                document.getElementById("bottom7markeriPad").style.display = 'none';
                document.getElementById("bottom8markeriPad").style.display = 'none';
                
                
                if (compItemArr[0].total_points >= 5) {
                    //          document.getElementById("bottom1markeriPad").style.display = 'block';
                    document.getElementById("bottom1markeriPad").style.width = '0px';
                }
                if (compItemArr[1].total_points >= 10) {
                    document.getElementById("bottom2markeriPad").style.display = 'block';
                    document.getElementById("bottom2markeriPad").style.width = '0px';
                }
                if (compItemArr[2].total_points >= 5) {
                    //            document.getElementById("bottom3markeriPad").style.display = 'block';
                    document.getElementById("bottom3markeriPad").style.width = '0px';
                }
                if (compItemArr[3].total_points >= 5) {
                    //           document.getElementById("bottom4markeriPad").style.display = 'block';
                    document.getElementById("bottom4markeriPad").style.width = '0px';
                }
                if (compItemArr[4].total_points >= 5) {
                    //           document.getElementById("bottom5markeriPad").style.display = 'block';
                    document.getElementById("bottom5markeriPad").style.width = '0px';
                }
                if (compItemArr[5].total_points >= 5) {
                    document.getElementById("bottom6markeriPad").style.display = 'none';
                    document.getElementById("bottom6markeriPad").style.width = '0px';
                }
                if (compItemArr[6].total_points >= 5) {
                    //         document.getElementById("bottom7markeriPad").style.display = 'block';
                    document.getElementById("bottom7markeriPad").style.width = '0px';
                }
                if (compItemArr[7].total_points >= 5) {
                    //       document.getElementById("bottom8markeriPad").style.display = 'block';
                    document.getElementById("bottom8markeriPad").style.width = '0px';
                }
                
            }
            else {
                document.getElementById("scorerow1").style.display = 'block';
                document.getElementById("scorerow2").style.display = 'block';
                document.getElementById("scorerow3").style.display = 'block';
                document.getElementById("scorerow4").style.display = 'block';
                document.getElementById("scorerow5").style.display = 'block';
                document.getElementById("scorerow6").style.display = 'block';
                document.getElementById("scorerow7").style.display = 'block';
                document.getElementById("scorerow8").style.display = 'block';
                document.getElementById("scorerowText").style.display = 'none';
                document.getElementById("bottom1marker").style.display = 'none';
                document.getElementById("bottom2marker").style.display = 'none';
                document.getElementById("bottom3marker").style.display = 'none';
                document.getElementById("bottom4marker").style.display = 'none';
                document.getElementById("bottom5marker").style.display = 'none';
                document.getElementById("bottom6marker").style.display = 'none';
                document.getElementById("bottom7marker").style.display = 'none';
                document.getElementById("bottom8marker").style.display = 'none';
                document.getElementById("bottom1marker").style.width = '0px';
                document.getElementById("bottom2marker").style.width = '0px';
                document.getElementById("bottom3marker").style.width = '0px';
                document.getElementById("bottom4marker").style.width = '0px';
                
            }
            
            
            animFlag = 'current';
            setTimeout('animateNumbers(999, animArrValue, animFlag)', 500);
        }
        
        else {
        
         
         Ext.getCmp('bottomScores').remove(Ext.getCmp('bottomScores').getComponent('secondPageScorePanel'));
         
         
            
            if (btnGrade == 'BRONZE') {
                document.getElementById("div_name_1").innerHTML = "Others";
                //  document.getElementById("div_name_2").innerHTML = "Others";
                
                document.getElementById("list_1").style.display = 'none';
                document.getElementById("list_2").style.display = 'none';
                
                document.getElementById("scorerowText").style.display = 'block';
                document.getElementById("scorerowText").innerHTML = "&nbsp;*Participation Points is capped at 10 pts.";
                
                
                document.getElementById("barDiv_1").innerHTML = newAnimArrValue[0] + otherComp;
                //  document.getElementById("barDiv_2").innerHTML = newAnimArrValue[1] + otherComp;
                
                if (newAnimArrValue[0] == 0) {
                    document.getElementById("barDiv_1").innerHTML = "Met target";
                }
                if (newAnimArrValue[1] == 0) {
                    document.getElementById("barDiv_2").innerHTML = "Met target";
                }
                
                if (screenWidth >= 700) {
                    document.getElementById("scorerow1iPad").style.display = 'block';
                    document.getElementById("scorerow2iPad").style.display = 'none';
                    document.getElementById("scorerow3iPad").style.display = 'none';
                    document.getElementById("scorerow4iPad").style.display = 'none';
                    document.getElementById("scorerow5iPad").style.display = 'none';
                    document.getElementById("scorerow6iPad").style.display = 'none';
                    document.getElementById("scorerow7iPad").style.display = 'none';
                    document.getElementById("scorerow8iPad").style.display = 'none';
                    //	document.getElementById("scorerowText").style.display = 'block';
                    //	document.getElementById("scorerowText").innerHTML = "&nbsp;*Participation Points is capped at 10 pts.";
                    document.getElementById("bottom1markeriPad").style.display = 'none';
                    document.getElementById("bottom2markeriPad").style.display = 'none';
                    document.getElementById("bottom3markeriPad").style.display = 'none';
                    document.getElementById("bottom4markeriPad").style.display = 'none';
                    document.getElementById("bottom5markeriPad").style.display = 'none';
                    document.getElementById("bottom6markeriPad").style.display = 'none';
                    document.getElementById("bottom7markeriPad").style.display = 'none';
                    document.getElementById("bottom8markeriPad").style.display = 'none';
                    
                    
                }
                else {
                    document.getElementById("scorerow1").style.display = 'block';
                    document.getElementById("scorerow2").style.display = 'none';
                    document.getElementById("scorerow3").style.display = 'none';
                    document.getElementById("scorerow4").style.display = 'none';
                    document.getElementById("bottom1marker").style.display = 'none';
                    document.getElementById("bottom2marker").style.display = 'none';
                    document.getElementById("bottom3marker").style.display = 'none';
                    document.getElementById("bottom4marker").style.display = 'none';
                }
                
                document.getElementById("ccaGradeDiv").innerHTML = 'Additional Points Required';
                
                
                
            }
            
            else 
                if (btnGrade == 'SILVER') {
                    //   document.getElementById("div_name_1").innerHTML = "Participation";
                    //   document.getElementById("div_name_2").innerHTML = "Community Svcs.";
                    //   document.getElementById("div_name_3").innerHTML = "Others";
                    
                    document.getElementById("div_name_1").innerHTML = "Community Svcs.";
                    document.getElementById("div_name_2").innerHTML = "Others";
                    
                    document.getElementById("list_2").style.display = 'none';
                    document.getElementById("list_1").style.display = 'block';
                    
                    document.getElementById("scorerowText").style.display = 'block';
                    document.getElementById("scorerowText").innerHTML = "&nbsp;*Participation Points is capped at 15 pts.";
                    
                    //document.getElementById("barDiv_1").innerHTML = newAnimArrValue[0] + parComp;
                    //document.getElementById("barDiv_2").innerHTML = newAnimArrValue[1] + serComp;
                    //document.getElementById("barDiv_3").innerHTML = newAnimArrValue[2] + otherComp;
                    
                    document.getElementById("barDiv_1").innerHTML = newAnimArrValue[0] + serComp;
                    document.getElementById("barDiv_2").innerHTML = newAnimArrValue[1] + otherComp;
                    
                    
                    if (newAnimArrValue[0] == 0) {
                        document.getElementById("barDiv_1").innerHTML = "Met target";
                    }
                    if (newAnimArrValue[1] == 0) {
                        document.getElementById("barDiv_2").innerHTML = "Met target";
                    }
                    if (newAnimArrValue[2] == 0) {
                        document.getElementById("barDiv_3").innerHTML = "Met target";
                    }
                    
                    if (screenWidth >= 700) {
                        document.getElementById("scorerow1iPad").style.display = 'block';
                        document.getElementById("scorerow2iPad").style.display = 'block';
                        document.getElementById("scorerow3iPad").style.display = 'none';
                        document.getElementById("scorerow4iPad").style.display = 'none';
                        document.getElementById("scorerow5iPad").style.display = 'none';
                        document.getElementById("scorerow6iPad").style.display = 'none';
                        document.getElementById("scorerow7iPad").style.display = 'none';
                        document.getElementById("scorerow8iPad").style.display = 'none';
                        document.getElementById("bottom1markeriPad").style.display = 'none';
                        document.getElementById("bottom2markeriPad").style.display = 'none';
                        document.getElementById("bottom3markeriPad").style.display = 'none';
                        document.getElementById("bottom4markeriPad").style.display = 'none';
                        document.getElementById("bottom5markeriPad").style.display = 'none';
                        document.getElementById("bottom6markeriPad").style.display = 'none';
                        document.getElementById("bottom7markeriPad").style.display = 'none';
                        document.getElementById("bottom8markeriPad").style.display = 'none';
                    }
                    else {
                        document.getElementById("scorerow1").style.display = 'block';
                        document.getElementById("scorerow2").style.display = 'block';
                        document.getElementById("scorerow3").style.display = 'none';
                        document.getElementById("scorerow4").style.display = 'none';
                        document.getElementById("bottom1marker").style.display = 'none';
                        document.getElementById("bottom2marker").style.display = 'none';
                        document.getElementById("bottom3marker").style.display = 'none';
                        document.getElementById("bottom4marker").style.display = 'none';
                    }
                    
                    
                    document.getElementById("ccaGradeDiv").innerHTML = 'Additional Points Required';
                }
                
                else {
                    //   document.getElementById("div_name_1").innerHTML = "Participation";
                    document.getElementById("div_name_1").innerHTML = "Community Svcs.";
                    document.getElementById("div_name_2").innerHTML = "Leadership/Achv.";
                    document.getElementById("div_name_3").innerHTML = "Others";
                    
                    document.getElementById("scorerowText").style.display = 'block';
                    document.getElementById("scorerowText").innerHTML = "&nbsp;*Participation Points is capped at 15 pts.";
                    
                    
                    if (screenWidth >= 700) {
                        document.getElementById("div_name_3").innerHTML = "Leadership/Achievement";
                    }
                    
                    
                    document.getElementById("list_1").style.display = 'block';
                    document.getElementById("list_2").style.display = 'block';
                    
                    //  document.getElementById("barDiv_1").innerHTML = newAnimArrValue[0] + parComp;
                    document.getElementById("barDiv_1").innerHTML = newAnimArrValue[0] + cosComp;
                    document.getElementById("barDiv_2").innerHTML = newAnimArrValue[1] + ledComp;
                    document.getElementById("barDiv_3").innerHTML = newAnimArrValue[2] + otherComp;
                    
                    if (newAnimArrValue[0] == 0) {
                        document.getElementById("barDiv_1").innerHTML = "Met target";
                    }
                    if (newAnimArrValue[1] == 0) {
                        document.getElementById("barDiv_2").innerHTML = "Met target";
                    }
                    if (newAnimArrValue[2] == 0) {
                        document.getElementById("barDiv_3").innerHTML = "Met target";
                    }
                    if (newAnimArrValue[3] == 0) {
                        document.getElementById("barDiv_4").innerHTML = "Met target";
                    }
                    
                    if (screenWidth >= 700) {
                    
                        if ((btnGrade == 'GOLD') || (btnGrade == 'HONOURS')) {
                            document.getElementById("scorerow1iPad").style.display = 'block';
                            document.getElementById("scorerow2iPad").style.display = 'block';
                            document.getElementById("scorerow3iPad").style.display = 'block';
                            document.getElementById("scorerow4iPad").style.display = 'none';
                            document.getElementById("scorerow5iPad").style.display = 'none';
                            document.getElementById("scorerow6iPad").style.display = 'none';
                            document.getElementById("scorerow7iPad").style.display = 'none';
                            document.getElementById("scorerow8iPad").style.display = 'none';
                        }
                        else {
                            document.getElementById("scorerow1iPad").style.display = 'block';
                            document.getElementById("scorerow2iPad").style.display = 'block';
                            document.getElementById("scorerow3iPad").style.display = 'block';
                            document.getElementById("scorerow4iPad").style.display = 'block';
                            document.getElementById("scorerow5iPad").style.display = 'block';
                            document.getElementById("scorerow6iPad").style.display = 'block';
                            document.getElementById("scorerow7iPad").style.display = 'block';
                            document.getElementById("scorerow8iPad").style.display = 'block';
                            
                            
                        }
                        
                        document.getElementById("bottom1markeriPad").style.display = 'none';
                        document.getElementById("bottom2markeriPad").style.display = 'none';
                        document.getElementById("bottom3markeriPad").style.display = 'none';
                        document.getElementById("bottom4markeriPad").style.display = 'none';
                        document.getElementById("bottom5markeriPad").style.display = 'none';
                        document.getElementById("bottom6markeriPad").style.display = 'none';
                        document.getElementById("bottom7markeriPad").style.display = 'none';
                        document.getElementById("bottom8markeriPad").style.display = 'none';
                        
                    }
                    else {
                        document.getElementById("scorerow1").style.display = 'block';
                        document.getElementById("scorerow2").style.display = 'block';
                        document.getElementById("scorerow3").style.display = 'block';
                        document.getElementById("scorerow4").style.display = 'none';
                        document.getElementById("bottom1marker").style.display = 'none';
                        document.getElementById("bottom2marker").style.display = 'none';
                        document.getElementById("bottom3marker").style.display = 'none';
                        document.getElementById("bottom4marker").style.display = 'none';
                    }
                    
                    
                    
                    if (btnGrade == 'GOLD') {
                        document.getElementById("ccaGradeDiv").innerHTML = 'Additional Points Required';
                    }
                    
                    if (btnGrade == 'HONOURS') {
                        document.getElementById("ccaGradeDiv").innerHTML = 'Additional Points Required';
                    }
                    
                    
                    
                }
            
            
            animFlag = 'none';
            
            setTimeout('animateNumbers(additionalPointsReqd, animArrValue, animFlag)', 500);
        }
        
        
    }
    else {
        setTimeout('animateNumbers(100, null, \'none\')', 500);
    }
    
    
}


String.prototype.replaceAll = function(search, replace){
    //if replace is null, return original string otherwise it will
    //replace search string with 'undefined'.
    if (!replace) 
        return this;
    
    return this.replace(new RegExp('[' + search + ']', 'g'), replace);
}


function getNumberOfZeroElems(elemArr){
    var zeroElem = 0;
    
    for (var randomNum = 0; randomNum < elemArr.length; randomNum++) {
        if (elemArr[randomNum] == 0) {
            zeroElem++;
        }
    }
    
    return zeroElem;
}

function getLengthOfElemnts(elemArr, itemPos){
    if (itemPos == 0) {
        var arrLen;
        
        if (elemArr[0] != 0) {
            arrLen = elemArr[0];
        }
        
        else {
            arrLen = 1;
        }
        
        return arrLen;
    }
    
    else {
        var prevsPos = itemPos - 1;
        var restArrLength = getLengthOfElemnts(elemArr, prevsPos);
        
        var arrLenNew;
        
        if (elemArr[itemPos] != 0) {
            arrLenNew = elemArr[itemPos];
        }
        
        else {
            arrLenNew = 1;
        }
        
        var arrLen = arrLenNew + restArrLength;
        
        return arrLen;
    }
}

function parseFromScoresString(xml){
    if (window.DOMParser) {
        return new DOMParser().parseFromString(xml, 'text/xml');
    }
    if (window.ActiveXObject) {
        var doc = new ActiveXObject('Microsoft.XMLDOM');
        doc.async = 'false';
        doc.load(xml);
        return doc;
    }
};




function loadSoapRequest(controller, studentId){

    Ext.Ajax.request({
      //  url: 'https://mobileweb.sp.edu.sg/ccaws/student/studentcca/ccastudentrequest.jsp',
        // url: 'data/ccastudentrequest.xml',
		 url : 'http://fritzllanora.com/mobile/ccastudentrequest.xml',
        success: function(response, opts){
            var responseStr = response.responseText;
            var modifiedXMLStr = responseStr.replace("<result><scheme>", "<result> <scheme>");
            modifiedXMLStr = modifiedXMLStr.replaceAll("&", "&amp;");
            var xmlDoc = new DOMParser().parseFromString(modifiedXMLStr, 'text/xml');
            console.log("FINAL XML: " + modifiedXMLStr);
            
            
            totalPoints = 100;
            try {
                totalPoints = parseInt(xmlDoc.getElementsByTagName('total-points')[0].childNodes[0].nodeValue) + 100;
            } 
            catch (err) {
            }
            
            ccaGrade = "NIL";
            try {
                ccaGrade = xmlDoc.getElementsByTagName('grade')[0].childNodes[0].nodeValue;
            } 
            catch (err) {
            
            }
            
            if ((ccaGrade == "NIL") || (ccaGrade == "BRONZE") || (ccaGrade == "SILVER") || (ccaGrade == "GOLD") || (ccaGrade == "GOLD with HONOURS")) {
            }
            else {
                ccaGrade = "NON-REVAMPED";
                totalPoints = 100;
                document.getElementById("ccaGradeDiv").innerHTML = 'This application is for the revamped CCA Points Awarding & Grading System.';
                
            }
            
            
            try {
                scheme = xmlDoc.getElementsByTagName('scheme')[0].childNodes[0].nodeValue;
            } 
            catch (err) {
            }
            
            if (scheme == "PRE-REVAMP") {
                ccaGrade = "NON-REVAMPED";
                totalPoints = 100;
                document.getElementById("ccaGradeDiv").innerHTML = 'This application is for the revamped CCA Points Awarding & Grading System.';
                
            }
            
            if (ccaGrade == "BRONZE") {
                document.getElementById("ccaGradeDiv").style.color = "#CD7F32";
            }
            else 
                if (ccaGrade == "SILVER") {
                    document.getElementById("ccaGradeDiv").style.color = "#C0C0C0";
                }
                else 
                    if (ccaGrade == "GOLD") {
                        document.getElementById("ccaGradeDiv").style.color = "#FFD700";
                    }
                    else 
                        if (ccaGrade == "GOLD with HONOURS") {
                            document.getElementById("ccaGradeDiv").style.color = "#FFD700";
                        }
                        else {
                            document.getElementById("ccaGradeDiv").style.color = "#FFFFFF";
                        }
            
            if ((totalPoints == 100) && (ccaGrade == "NIL")) {
                document.getElementById("ccaGradeDiv").innerHTML = 'You have not achieved any CCA Scores.';
                setTimeout('animateNumbers(100, null, \'none\')', 500);
                Ext.getCmp('HomeCCAScoresPanel').setActiveItem(3);
                
            }
            if ((totalPoints == 100) && (ccaGrade == "NON-REVAMPED")) {
                document.getElementById("machine").style.display = "none";
                document.getElementById("machine_below").style.display = "block";
                Ext.getCmp('HomeCCAScoresPanel').setActiveItem(3);
            }
            
            
            if (totalPoints > 100) {
            
                var compArr = xmlDoc.getElementsByTagName('components')[0].childNodes;
                var particArr = xmlDoc.getElementsByTagName('components')[0].childNodes[0].childNodes;
                
                itemArray = [];
                itemArray[0] = parseInt((xmlDoc.childNodes[0].childNodes[6].childNodes[0].childNodes.length - 1) / 5);
                itemArray[1] = parseInt((xmlDoc.childNodes[0].childNodes[6].childNodes[1].childNodes.length - 1) / 5);
                itemArray[2] = parseInt((xmlDoc.childNodes[0].childNodes[6].childNodes[2].childNodes.length - 1) / 5);
                itemArray[3] = parseInt((xmlDoc.childNodes[0].childNodes[6].childNodes[3].childNodes.length - 1) / 5);
                itemArray[4] = parseInt((xmlDoc.childNodes[0].childNodes[6].childNodes[4].childNodes.length - 1) / 5);
                itemArray[5] = parseInt((xmlDoc.childNodes[0].childNodes[6].childNodes[5].childNodes.length - 1) / 5);
                itemArray[6] = parseInt((xmlDoc.childNodes[0].childNodes[6].childNodes[6].childNodes.length - 1) / 5);
                itemArray[7] = parseInt((xmlDoc.childNodes[0].childNodes[6].childNodes[7].childNodes.length - 1) / 5);
                
                
                var itemCounter = 0;
                
                for (var c = 0; c < compArr.length; c++) {
                    lArr = [];
                    var counter = 0;
                    
                    var compnentHash = new Object;
                    compnentHash.component_name = xmlDoc.childNodes[0].childNodes[6].childNodes[c].nodeName;
                    
                    for (var p = 0; p < itemArray[c]; p++) {
                        if (parseInt(xmlDoc.getElementsByTagName('points-component')[c].childNodes[0].nodeValue) != 0) {
                            if (xmlDoc.childNodes[0].childNodes[6].childNodes[7].childNodes.nodeName != 'points-component') {
                                var itemPosition;
                                
                                if (c != 0) {
                                    var itemPos = c - 1;
                                    var itemRefPosition = getLengthOfElemnts(itemArray, itemPos);
                                    itemPosition = p + itemRefPosition;
                                }
                                
                                else {
                                    itemPosition = p;
                                }
                                
                                if (xmlDoc.getElementsByTagName('points-component')[c].childNodes[0].nodeValue != 0) {
                                    var lItemHash = new Object;
                                    lItemHash.activity = xmlDoc.getElementsByTagName('activity')[itemPosition].childNodes[0].nodeValue;
                                    
                                    lItemHash.rolename = xmlDoc.getElementsByTagName('rolename')[itemCounter].childNodes[0].nodeValue;
                                    lItemHash.start_date = xmlDoc.getElementsByTagName('start-date')[itemCounter].childNodes[0].nodeValue;
                                    lItemHash.end_date = xmlDoc.getElementsByTagName('end-date')[itemCounter].childNodes[0].nodeValue;
                                    lItemHash.points_activity = xmlDoc.getElementsByTagName('points-activity')[itemCounter].childNodes[0].nodeValue;
                                    
                                    itemCounter++;
                                    
                                    
                                    lArr[counter] = lItemHash;
                                }
                            }
                            counter++;
                        }
                    }
                    
                    compnentHash.component_value = lArr;
                    compnentHash.total_points = xmlDoc.getElementsByTagName('points-component')[c].childNodes[0].nodeValue;
                    
                    compItemArr[c] = compnentHash;
                }
            }
          // drawTopSegments();
          drawTopSegments(controller);
          resetBottomContainer(ccaGrade);

            
             
        },
        failure: function(response, opts){
            //alert("failure");
        }
    });
}



function animateNumbers(points, animArrValue, animFlag){
    if (points != 999) {
        var animNum1 = points;
    }
    
    else {
        var animNum1 = totalPoints;
    }
    
    var actualNum = animNum1 - 100;
    var numArr;
    
    if (actualNum >= 100) {
        numArr = actualNum.toString().split('');
    }
    
    else {
        if (actualNum >= 10) {
            numArr = [0, actualNum.toString().split('')[0], actualNum.toString().split('')[1]];
        }
        
        else {
            numArr = [0, 0, actualNum.toString().split('')[0]];
        }
    }
    
    var a = 5 + (numArr[0] * 28);
    var b = 5 + (numArr[1] * 28);
    var c = 5 + (numArr[2] * 28);
    
    document.getElementById("machine").className = "test";
    document.getElementById("a").style.backgroundPosition = "0," + " 0 -" + a + "px";
    document.getElementById("b").style.backgroundPosition = "0," + " 0 -" + b + "px";
    document.getElementById("c").style.backgroundPosition = "0," + " 0 -" + c + "px";
    
    if (animFlag != 'none') {
        animateBar(0, 'none', animArrValue, 'current');
    }
    
    else {
        animateBar(0, 'none', animArrValue, 'none');
    }
    
}


function animateBar(activityIndex, animationDirection, animArrValue, animFlag){
    beginAnimate(animArrValue, animFlag);
}

function deActivateAnim(){

    if (screenWidth >= 700) {
        document.getElementById('bottomiPad').style.zIndex = 98;
        document.getElementById('bottom2iPad').style.zIndex = 98;
        document.getElementById('bottom3iPad').style.zIndex = 98;
        document.getElementById('bottom4iPad').style.zIndex = 98;
        document.getElementById('bottomiPad').style.width = '0px';
        document.getElementById('bottom2iPad').style.width = '0px';
        document.getElementById('bottom3iPad').style.width = '0px';
        document.getElementById('bottom4iPad').style.width = '0px';
        
        
        if (animFlag != 'none') {
            document.getElementById('bottom5iPad').style.zIndex = 98;
            document.getElementById('bottom6iPad').style.zIndex = 98;
            document.getElementById('bottom7iPad').style.zIndex = 98;
            document.getElementById('bottom8iPad').style.zIndex = 98;
            document.getElementById('bottom5iPad').style.width = '0px';
            document.getElementById('bottom6iPad').style.width = '0px';
            document.getElementById('bottom7iPad').style.width = '0px';
            document.getElementById('bottom8iPad').style.width = '0px';
        }
    }
    else {
        document.getElementById('bottom').style.zIndex = 98;
        document.getElementById('bottom2').style.zIndex = 98;
        document.getElementById('bottom3').style.zIndex = 98;
        document.getElementById('bottom4').style.zIndex = 98;
        document.getElementById('bottom').style.width = '0px';
        document.getElementById('bottom2').style.width = '0px';
        document.getElementById('bottom3').style.width = '0px';
        document.getElementById('bottom4').style.width = '0px';
        
        if (animFlag != 'none') {
            document.getElementById('bottom5').style.zIndex = 98;
            document.getElementById('bottom6').style.zIndex = 98;
            document.getElementById('bottom7').style.zIndex = 98;
            document.getElementById('bottom8').style.zIndex = 98;
            document.getElementById('bottom5').style.width = '0px';
            document.getElementById('bottom6').style.width = '0px';
            document.getElementById('bottom7').style.width = '0px';
            document.getElementById('bottom8').style.width = '0px';
        }
    }
}



function beginAnimate(animArrValue, animFlag){

    if (screenWidth >= 700) {
        document.getElementById('bottomiPad').style.zIndex = 0;
        document.getElementById('bottomiPad').style.width = parseInt(animArrValue[0] * 440) + 'px';
        document.getElementById('bottom2iPad').style.zIndex = 0;
        document.getElementById('bottom2iPad').style.width = parseInt(animArrValue[1] * 440) + 'px';
        document.getElementById('bottom3iPad').style.zIndex = 0;
        document.getElementById('bottom3iPad').style.width = parseInt(animArrValue[2] * 440) + 'px';
        document.getElementById('bottom4iPad').style.zIndex = 0;
        document.getElementById('bottom4iPad').style.width = parseInt(animArrValue[3] * 440) + 'px';
        document.getElementById('bottom1markeriPad').style.width = '2px';
        document.getElementById('bottom1markeriPad').style.left = (parseInt(440 / compItemArr[0].total_points) * 5) + 'px';
        
        document.getElementById('bottom2markeriPad').style.width = '2px';
        document.getElementById('bottom2markeriPad').style.left = (parseInt(440 / compItemArr[1].total_points) * 10) + 'px';
        
        document.getElementById('bottom3markeriPad').style.width = '2px';
        document.getElementById('bottom3markeriPad').style.left = (parseInt(440 / compItemArr[2].total_points) * 5) + 'px';
        
        document.getElementById('bottom4markeriPad').style.width = '2px';
        document.getElementById('bottom4markeriPad').style.left = (parseInt(440 / compItemArr[3].total_points) * 5) + 'px';
        
        if (compItemArr[0].total_points == 0) {
            document.getElementById("bottom1markeriPad").style.display = 'none';
        }
        if (compItemArr[1].total_points == 0) {
            document.getElementById("bottom2markeriPad").style.display = 'none';
        }
        if (compItemArr[2].total_points == 0) {
            document.getElementById("bottom3markeriPad").style.display = 'none';
        }
        if (compItemArr[3].total_points == 0) {
            document.getElementById("bottom4markeriPad").style.display = 'none';
        }
        
        
    }
    else {
    
    
        document.getElementById('bottom').style.zIndex = 0;
        document.getElementById('bottom').style.width = parseInt(animArrValue[0] * 228) + 'px';
        document.getElementById('bottom2').style.zIndex = 0;
        document.getElementById('bottom2').style.width = parseInt(animArrValue[1] * 228) + 'px';
        document.getElementById('bottom3').style.zIndex = 0;
        document.getElementById('bottom3').style.width = parseInt(animArrValue[2] * 228) + 'px';
        document.getElementById('bottom4').style.zIndex = 0;
        document.getElementById('bottom4').style.width = parseInt(animArrValue[3] * 228) + 'px';
        document.getElementById('bottom1marker').style.width = '2px';
        document.getElementById('bottom1marker').style.left = (parseInt(228 / compItemArr[0].total_points) * 5) + 'px';
        
        document.getElementById('bottom2marker').style.width = '2px';
        document.getElementById('bottom2marker').style.left = (parseInt(228 / compItemArr[1].total_points) * 10) + 'px';
        
        document.getElementById('bottom3marker').style.width = '2px';
        document.getElementById('bottom3marker').style.left = (parseInt(228 / compItemArr[2].total_points) * 5) + 'px';
        
        document.getElementById('bottom4marker').style.width = '2px';
        document.getElementById('bottom4marker').style.left = (parseInt(228 / compItemArr[3].total_points) * 5) + 'px';
        
        if (compItemArr[0].total_points == 0) {
            document.getElementById("bottom1marker").style.display = 'none';
        }
        if (compItemArr[1].total_points == 0) {
            document.getElementById("bottom2marker").style.display = 'none';
        }
        if (compItemArr[2].total_points == 0) {
            document.getElementById("bottom3marker").style.display = 'none';
        }
        if (compItemArr[3].total_points == 0) {
            document.getElementById("bottom4marker").style.display = 'none';
        }
        
        if (animArrValue[1] >= 1) {
            document.getElementById("bottom2marker").style.display = 'none';
        }
        
        if (animArrValue[5] >= 1) {
            document.getElementById("bottom6marker").style.display = 'none';
        }
        
        
        
        
    }
    
    
    
    
    if (animFlag != 'none') {
    
    
    
        if (screenWidth >= 700) {
            document.getElementById('bottom5iPad').style.zIndex = 0;
            document.getElementById('bottom5iPad').style.width = parseInt(animArrValue[4] * 440) + 'px';
            document.getElementById('bottom6iPad').style.zIndex = 0;
            document.getElementById('bottom6iPad').style.width = parseInt(animArrValue[5] * 440) + 'px';
            document.getElementById('bottom7iPad').style.zIndex = 0;
            document.getElementById('bottom7iPad').style.width = parseInt(animArrValue[6] * 440) + 'px';
            document.getElementById('bottom8iPad').style.zIndex = 0;
            document.getElementById('bottom8iPad').style.width = parseInt(animArrValue[7] * 440) + 'px';
            document.getElementById('bottom5markeriPad').style.width = '2px';
            document.getElementById('bottom5markeriPad').style.left = (parseInt(440 / compItemArr[4].total_points) * 5) + 'px';
            
            document.getElementById('bottom6markeriPad').style.width = '2px';
            document.getElementById('bottom6markeriPad').style.left = (parseInt(440 / compItemArr[5].total_points) * 5) + 'px';
            
            document.getElementById('bottom7markeriPad').style.width = '2px';
            document.getElementById('bottom7markeriPad').style.left = (parseInt(440 / compItemArr[6].total_points) * 5) + 'px';
            
            document.getElementById('bottom8markeriPad').style.width = '2px';
            document.getElementById('bottom8markeriPad').style.left = (parseInt(440 / compItemArr[7].total_points) * 5) + 'px';
            
            if (compItemArr[4].total_points == 0) {
                document.getElementById("bottom5markeriPad").style.display = 'none';
            }
            if (compItemArr[5].total_points == 0) {
                document.getElementById("bottom6markeriPad").style.display = 'none';
            }
            if (compItemArr[6].total_points == 0) {
                document.getElementById("bottom7markeriPad").style.display = 'none';
            }
            if (compItemArr[7].total_points == 0) {
                document.getElementById("bottom8markeriPad").style.display = 'none';
            }
            
        }
        else {
            document.getElementById('bottom5').style.zIndex = 0;
            document.getElementById('bottom5').style.width = parseInt(animArrValue[4] * 228) + 'px';
            document.getElementById('bottom6').style.zIndex = 0;
            document.getElementById('bottom6').style.width = parseInt(animArrValue[5] * 228) + 'px';
            document.getElementById('bottom7').style.zIndex = 0;
            document.getElementById('bottom7').style.width = parseInt(animArrValue[6] * 228) + 'px';
            document.getElementById('bottom8').style.zIndex = 0;
            document.getElementById('bottom8').style.width = parseInt(animArrValue[7] * 228) + 'px';
            document.getElementById('bottom5marker').style.width = '2px';
            document.getElementById('bottom5marker').style.left = (parseInt(228 / compItemArr[4].total_points) * 5) + 'px';
            
            document.getElementById('bottom6marker').style.width = '2px';
            document.getElementById('bottom6marker').style.left = (parseInt(228 / compItemArr[5].total_points) * 5) + 'px';
            
            document.getElementById('bottom7marker').style.width = '2px';
            document.getElementById('bottom7marker').style.left = (parseInt(228 / compItemArr[6].total_points) * 5) + 'px';
            
            document.getElementById('bottom8marker').style.width = '2px';
            document.getElementById('bottom8marker').style.left = (parseInt(228 / compItemArr[7].total_points) * 5) + 'px';
            
            if (compItemArr[4].total_points == 0) {
                document.getElementById("bottom5marker").style.display = 'none';
            }
            if (compItemArr[5].total_points == 0) {
                document.getElementById("bottom6marker").style.display = 'none';
            }
            if (compItemArr[6].total_points == 0) {
                document.getElementById("bottom7marker").style.display = 'none';
            }
            if (compItemArr[7].total_points == 0) {
                document.getElementById("bottom8marker").style.display = 'none';
            }
            
            
            if (animArrValue[1] >= 1) {
                document.getElementById("bottom2marker").style.display = 'block';
            }
            
        }
    }
}




