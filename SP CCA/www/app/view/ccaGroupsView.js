Ext.define('app.view.ccaGroupsView', {
    extend: 'Ext.Panel',
    xtype: 'ccaGroupsView',
    
   requires: [
              'Ext.TitleBar',
       'app.view.ccaGroupsList',
   ],

           
  config:       {
           title: 'CCA Groups',
           iconCls: 'user',
           layout: 'fit',
           items:[
                  {
                  xtype: 'titlebar',
                  title: 'CCA Groups',
                  docked: 'top',  items: [
                                          {
                                          iconCls: 'settings',
                                          ui: 'plain',
                                          iconMask: true,
                                          align: 'right',
										     id: 'settingsBtnc',
										   	 listeners: {
            

        initialize: function(){ 
			if(device.platform=='Win32NT')
					   {
					  Ext.getCmp('settingsBtnc').setIconCls('settings win');
					   }
		
		
		}},
                                          
                                          handler: function() {
                                          displaySettingsPanel(this);
                                          }}]
                  
                  },
                  {
                    xtype: 'ccaGroupsList'
                  }
                  ]
           
           }
 
	

});


