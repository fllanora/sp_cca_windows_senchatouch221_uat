Ext.define('app.view.ccaGroupsList', {
    extend: 'Ext.List',

    xtype: 'ccaGroupsList',
    
    requires: [
        'app.store.ccaGroupsStore'
    ],

    config: {
        cls: 'customHeader',
        emptyText: 'No data found!',
        store: 'ccaGroupsStore',
		grouped: true,
		indexBar: true,
        itemTpl : [
			'<div style="float:left; font-size:11px;">{name}<br/><span style="font-size:10px;">URL: http://{url_addr}</span></div><img style="text-align: center; float:right; margin-right:30px; margin-top:5px;" src="app/view/images/arrow_right.png" >'].join('')

	}

});
