Ext.define('app.store.ccaComponentsStore', {
           extend: 'Ext.data.Store',
           
           
           requires: [
                      'app.model.ccaComponentsModel'
                      ],
           
           config: {
           model: 'app.model.ccaComponentsModel',
           autoLoad: true,
           
           grouper: {groupFn: function(record) {
           return record.get('activity')[0];
           }},

           sorters: [{
                     property : "activity",
                     direction: "ASC"
                     }],
           }
           });